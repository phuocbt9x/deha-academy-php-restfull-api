<?php

namespace Tests\Feature\Author;

use App\Models\Author;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateAuthorTest extends TestCase
{
    use WithFaker;

    /** @test*/
    public function user_can_update_author_if_data_validate(){
        $author = Author::all()->random()->first();
        $data = [
            'name' => $this->faker->name
        ];
        $response = $this->putJson(route('api.authors.update', $author->id), $data);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->where('statusCode', Response::HTTP_OK)
                ->has('data')
                ->etc()
        );

        $this->assertDatabaseHas('authors',[
            'name' => $data['name']
        ]);
    }

    /** @test*/
    public function user_can_not_update_author_if_data_not_validate(){
        $author = Author::all()->random()->first();
        $data = [
            'name' => ''
        ];
        $response = $this->putJson(route('api.authors.update', $author->id), $data);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->where('statusCode', Response::HTTP_UNPROCESSABLE_ENTITY)
            ->has('message')
            ->has('errors')
            ->etc()
        );
    }

    /** @test*/
    public function user_can_not_update_author_if_author_not_exists(){
        $data = [
            'name' => ''
        ];
        $response = $this->putJson(route('api.authors.update', -1), $data);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->where('statusCode', Response::HTTP_NOT_FOUND)
            ->has('message')
            ->etc()
        );
    }
}
