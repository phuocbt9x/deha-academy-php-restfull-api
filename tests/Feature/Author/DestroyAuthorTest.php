<?php

namespace Tests\Feature\Author;

use App\Models\Author;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DestroyAuthorTest extends TestCase
{
    /** @test*/
    public function user_can_destroy_author_if_author_exists(){
        $author = Author::all()->random()->first();
        $response = $this->deleteJson(route('api.authors.update', $author->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->where('statusCode', Response::HTTP_NO_CONTENT)
            ->has('message')
            ->etc()
        );
    }

    /** @test*/
    public function user_can_not_destroy_author_if_author_not_exists(){
        $response = $this->putJson(route('api.authors.update', -1));
        $response->assertJson(fn(AssertableJson $json) =>
        $json->where('statusCode', Response::HTTP_NOT_FOUND)
            ->has('message')
            ->etc()
        );
    }
}
