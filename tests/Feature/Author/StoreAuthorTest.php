<?php

namespace Tests\Feature\Author;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class StoreAuthorTest extends TestCase
{
    use WithFaker;

    /** @test*/
    public function user_can_create_author_if_data_validate(){
        $data = [
            'name' => $this->faker->name,
            'gender' => $this->faker->boolean,
            'birthday' => $this->faker->date,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'address' => $this->faker->address,
            'activated' => $this->faker->boolean
        ];
        $response = $this->postJson(route('api.authors.store'), $data);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data', fn(AssertableJson $json) =>
                $json->where('name', $data['name'])
                    ->etc()
            )->etc()
        );

        $this->assertDatabaseHas('authors',[
            'name' => $data['name'],
            'gender' => $data['gender'],
            'birthday' => $data['birthday'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'activated' => $data['activated'],
        ]);
    }

    /** @test*/
    public function user_can_not_create_author_if_data_not_validate(){
        $data = [
            'name' => $this->faker->name,
            'gender' => $this->faker->boolean
        ];
        $response = $this->postJson(route('api.authors.store'), $data);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->where('statusCode', Response::HTTP_UNPROCESSABLE_ENTITY)
                ->has('message')
                ->has('errors')
                ->etc()
        );
    }
}
