<?php

namespace Tests\Feature\Author;

use App\Models\Author;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class ShowAuthorTest extends TestCase
{
    /** @test*/
    public function user_can_get_author_if_author_exists(){
        $author = Author::factory()->create();
        $response = $this->getJson(route('api.authors.show', $author->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data', fn(AssertableJson $json) =>
                $json->where('name', $author->name)
                    ->etc()
            )
            ->etc()
        );

        $this->assertDatabaseHas('authors', [
            'name' => $author->name,
            'gender' => $author->gender,
            'birthday' =>  $author->birthday,
            'email' => $author->email,
            'phone' => $author->phone,
            'address' => $author->address,
            'activated' => $author->activated
        ]);
    }

    /** @test*/
    public function user_can_not_get_author_if_author_not_exists(){

        $response = $this->getJson(route('api.authors.show', -1));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->where('statusCode', Response::HTTP_NOT_FOUND)
                ->has('message')
        );
    }
}
