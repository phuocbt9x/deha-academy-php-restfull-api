<?php

namespace App\Http\Resources\Author;

use App\Http\Resources\Post\PostCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'gender' => $this->gender,
            'birthday' => $this->birthday,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'posts' => new PostCollection($this->posts),
            'activated' => $this->activated
        ];
    }
}
