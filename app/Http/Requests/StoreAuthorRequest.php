<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class StoreAuthorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5|max:200',
            'gender' => 'required|boolean',
            'birthday' => 'required|date|before:now',
            'email' => 'required|email|unique:authors,email',
            'phone' => 'required|unique:authors,phone',
            'address' => 'sometimes',
            'activated' => 'sometimes|boolean'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $reponse = response()->json([
            'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY,
            'message' => 'Error validation',
            'errors' => $validator->errors()
        ]);
        throw (new ValidationException($validator, $reponse));
    }
}
