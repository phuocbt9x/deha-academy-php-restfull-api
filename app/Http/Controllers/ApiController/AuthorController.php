<?php

namespace App\Http\Controllers\ApiController;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAuthorRequest;
use App\Http\Requests\UpdateAuthorRequest;
use App\Http\Resources\Author\AuthorCollection;
use App\Http\Resources\Author\AuthorResource;
use App\Models\Author;
use Illuminate\Http\Response;

class AuthorController extends Controller
{
    protected $author;

    public function __construct(Author $author)
    {
        $this->author = $author;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function index()
    {
        $authors = $this->author->paginate(5);
        $response = new AuthorCollection($authors);
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'message' => 'List all authors.',
            'data' => $response
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAuthorRequest  $request
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function store(StoreAuthorRequest $request)
    {
        $author = $this->author->create($request->validated());
        $response = new AuthorResource($author);
        return response()->json([
            'statusCode' => Response::HTTP_CREATED,
            'message' => 'Create new author.',
            'data' => $response
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function show(Author $author)
    {
        $response = new AuthorResource($author);
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'message' => 'Detail a author.',
            'data' => $response
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAuthorRequest  $request
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateAuthorRequest $request, Author $author)
    {
        $author->update($request->validated());
        $response = new AuthorResource($author);
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'message' => 'Update a author.',
            'data' => $response
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function destroy(Author $author)
    {
        $author->delete();
        return response()->json([
            'statusCode' => Response::HTTP_NO_CONTENT,
            'message' => 'Delete a author.'
        ]);
    }
}
